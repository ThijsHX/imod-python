Roadmap
=======

Here we plan to describe our ideas of how we want to evolve the package, including our philosophy and general vision.

We can take inspiration from the `xarray roadmap <https://xarray.pydata.org/en/stable/roadmap.html>`_.
