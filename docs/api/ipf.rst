imod.ipf  -  IPF file I/O
-------------------------

.. automodule:: imod.ipf
    :members:
    :undoc-members:
    :show-inheritance:
